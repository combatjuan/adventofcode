#!/usr/bin/env bash

npm install --save-dev @types/node

mkdir -p data
echo "Hello, Advent of Code!" > data/00.example

mkdir -p src
cat > src/day00.ts <<EOF
import * as fs from 'fs';
fs.readFile('data/00.example', function (err, data) {
	if (err) {
		console.error(err);
	} else {
		console.log(data.toString());
	}
});
EOF

tsc src/day00.ts
node src/day00.js
